```plantuml
@startuml
class Patinet{
MedicalCard medicalCard
}

class Doctor{
string fio_doctor
string password
void Sign_in()
void Close_Session()
}

class HospitalSystem{
List<Patinet> patients
List<Doctor> doctors
MedicalCard GetMedicalCard(string fio)
void CreateReception(GetMedicalCard())
void AddComplaints()
void AddMedicine()
}

class MedicalCard{
string fio_patient
DateTime birthday
int age
List<Reception> receptions
void Print()
}

class Reception{
DateTime visit
string complaints
string medicine
}


Doctor -up- HospitalSystem
HospitalSystem -- Patinet
Patinet -- MedicalCard
MedicalCard -right- Reception
@enduml
```
